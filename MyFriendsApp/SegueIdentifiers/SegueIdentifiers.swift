//
//  SegueIdentifiers.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import Foundation

struct SegueIdentifiers {
    static let mainToEdit = "mainToEdit"
    static let unwindSegueToVC1 = "unwindSegueToVC1"
}
