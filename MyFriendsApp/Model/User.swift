//
//  User.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit

struct User {
    
    var id: String
    var imageUrl: URL
    var firstName: String
    var lastName: String
    var phoneNumber: String
    var email: String
    var coordinates: String
    var isFriend: Bool
}

extension User : Equatable {
    static func ==(lhs: User, rhs: User) -> Bool {
        if  lhs.id == rhs.id  {
            return true
        }
        return false
    }
    
    var location: (latitude: Double, longitude: Double) {
        
        let indexOfSeparator = self.coordinates.characters.index(of: ":")!
        let latitude = Double(self.coordinates.substring(to: indexOfSeparator))!
        let longitude = Double(self.coordinates.substring(from: self.coordinates.characters.index(after: indexOfSeparator)))!

        return (latitude, longitude)
    }
}
