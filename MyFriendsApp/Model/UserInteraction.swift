//
//  UserInteraction.swift
//  MyFriendsApp
//
//  Created by Sem Vastuin on 03.03.17.
//  Copyright © 2017 Sem Vastuin. All rights reserved.
//

import Foundation

protocol UserInteraction {
    func add(user: User)
    func edit(user: User)
    func delete(user: User)
}
