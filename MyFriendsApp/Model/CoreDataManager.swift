//
//  CoreDataManager.swift
//  MyFriendsApp
//
//  Created by Sem Vastuin on 03.03.17.
//  Copyright © 2017 Sem Vastuin. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class CoreDataManager {
    
    // MARK: - Singleton
    static let instanse = CoreDataManager()
    private init() {
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
    }
    
    func fetchData(_ complition: @escaping (([User]) -> Void)) {
        self.persistentContainer.performBackgroundTask { context in
            let personList = try! context.fetch(Person.fetchRequest()) as! [Person]
            var userList = [User]()
            for person in personList {
                let id = person.id
                let imageUrl = URL(string:person.imageUrl!)
                let firstName = person.firstName
                let lastName = person.lastName
                let phoneNumber = person.phoneNumber
                let email = person.email
                let isFriend = person.isFriend
                let coordinate = person.coordinate
                
                let user = User(id: id!, imageUrl: imageUrl!, firstName: firstName!, lastName: lastName!, phoneNumber: phoneNumber!, email: email!, coordinates: coordinate!, isFriend: isFriend)
                userList.append(user)
            }
            complition(userList)
        }
    }

    // MARK: - Core Data stack
    fileprivate lazy var persistentContainer: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "MyFriendsApp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

// MARK: - UserInteraction protocol methods
extension CoreDataManager : UserInteraction {
    
    // TODO: Need to refactor
    func add(user: User) {
        self.persistentContainer.performBackgroundTask{ context in
            let person = Person(context: context)
            person.id = user.id
            person.imageUrl = String(describing: user.imageUrl)
            person.firstName = user.firstName
            person.lastName = user.lastName
            person.phoneNumber = user.phoneNumber
            person.email = user.email
            person.isFriend = true
            try? context.save()
        }
    }
    
    func edit(user: User) {
        self.persistentContainer.performBackgroundTask{ context in
            let person = try! context.fetch(Person.fetchRequest()).filter{ $0.id == user.id}.first!
            person.imageUrl = String(describing: user.imageUrl)
            person.firstName = user.firstName
            person.lastName = user.lastName
            person.phoneNumber = user.phoneNumber
            person.email = user.email
            
            try? context.save()
        }
    }
    
    func delete(user: User) {
        self.persistentContainer.performBackgroundTask{ context in
            let person = try! context.fetch(Person.fetchRequest()).filter{ $0.id == user.id }.first!
            person.isFriend = false
            
            try? context.save()
        }
    }
}
