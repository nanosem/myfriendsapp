//
//  NetworkDataManager.swift
//  MyFriendsApp
//
//  Created by Sem Vastuin on 22.03.17.
//  Copyright © 2017 Sem Vastuin. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import FirebaseStorage

class NetworkDataManager {
    static let instance = NetworkDataManager()
    private init() {}
    
    fileprivate let ref = FIRDatabase.database().reference(fromURL: "https://myfriendsapp-dced5.firebaseio.com/")
    
    func fetchData(_ complition: @escaping (([User]) -> Void)) {
        FIRDatabase.database().reference().child("users").observeSingleEvent(of: .value, with: { (snapshot) in
            var userList = [User]()
            if let dictionary = snapshot.value as? [String: AnyObject] {
                
                for index in 0..<dictionary.count {
                    let currentKey = dictionary.keys.sorted()[index]
                    let currentUser = dictionary[currentKey]

                    let id          = currentUser?["id"] as! String
                    let firstName   = currentUser?["firstName"] as! String
                    let lastName    = currentUser?["lastName"] as! String
                    let phoneNumber = currentUser?["phoneNumber"] as! String
                    let email       = currentUser?["email"] as! String
                    let imageUrl    = URL(string: currentUser?["imageUrl"] as! String)
                    let coordinates = currentUser?["coordinates"] as! String
                    let isFriend    = currentUser?["isFriend"] as! Bool
                    
                        let user = User(id: id, imageUrl: imageUrl!, firstName: firstName, lastName: lastName, phoneNumber:phoneNumber, email: email, coordinates: coordinates, isFriend: isFriend)
                    
                    userList.append(user)
                }
            }
            complition(userList)
        })
    }
    
    func uploadImageToStorageForUserWithID(_ userID: String, image: UIImage) -> URL {
        
        let storageRef = FIRStorage.storage().reference(forURL: "gs://myfriendsapp-dced5.appspot.com/").child("\(userID)")
        
        if let uploadData = UIImagePNGRepresentation(image) {
            storageRef.put(uploadData, metadata: nil, completion: nil)
        }
        
        return URL(string: "gs://myfriendsapp-dced5.appspot.com/\(userID)")!
    }
}

// MARK: - UserInteraction methods
extension NetworkDataManager: UserInteraction {
    
    func add(user: User) {
        ref.child("users/\(user.id)").setValue(["id": user.id,
                                                          "firstName": user.firstName,
                                                          "lastName": user.lastName,
                                                          "phoneNumber": user.phoneNumber,
                                                          "email": user.email,
                                                          "imageUrl": "\(user.imageUrl)",
                                                          "coordinates": user.coordinates,
                                                          "isFriend": true])
    }
    
    func edit(user: User) {
        ref.child("users/\(user.id)").setValue(["id": user.id,
                                                          "firstName": user.firstName,
                                                          "lastName": user.lastName,
                                                          "phoneNumber": user.phoneNumber,
                                                          "email": user.email,
                                                          "imageUrl": "\(user.imageUrl)",
                                                          "coordinates": user.coordinates,
                                                          "isFriend": user.isFriend])
    }
    
    func delete(user: User) {
       ref.child("users/\(user.id)/isFriend").setValue(false)
    }
}
