//
//  DataManager.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import SwiftyJSON

class DataManager {
    
    // MARK: - Singleton
    static let instanse = DataManager()
    private init() {}
    
    private(set) var friendList = [User]()
    private(set) var userList = [User]() {
        didSet {
            friendList = userList.filter{ $0.isFriend }
        }
    }
    
    // MARK: - DataManager methods
    func loadUserList(_ complition: @escaping (([User]) -> Void)) {
        Alamofire.request("https://randomuser.me/api/?results=50").responseJSON { response in
            if let value = response.data {
            var users = [User]()
            let json = JSON(data: value)
            let group = DispatchGroup()
                
            for userInfo in json["results"].array! {
                group.enter()
                let imageUrl    = URL(string: userInfo["picture"]["large"].stringValue)
                let firstName   = userInfo["name"]["first"].stringValue
                let lastName    = userInfo["name"]["last"].stringValue
                let phoneNumber = userInfo["phone"].stringValue
                let email       = userInfo["email"].stringValue
                let identifier  = Utils.randomString
                
                let location = "\(Utils.randomLocation.latitude):\(Utils.randomLocation.longitude)"
                
                let user = User(id: identifier,
                                imageUrl: imageUrl!,
                                firstName: firstName.capitalizingFirstLetter(),
                                lastName: lastName.capitalizingFirstLetter(),
                                phoneNumber: phoneNumber,
                                email: email,
                                coordinates: location,
                                isFriend: false)
            
                    users.append(user)
                    group.leave()
                }
                group.notify(queue: DispatchQueue.main) {
                    complition(users)
                }
            }
        }
    }
    
    func loadFriendList() {
        NetworkDataManager.instance.fetchData { [weak self] users in
            self?.userList = users
            NotificationCenter.default.post(name: .friendListLoaded, object: nil)
        }
    }
    
    // MARK: - Private methods 
    fileprivate func addFriendLocaly(_ user: User) {
        var currentUser = user
        currentUser.isFriend = true
        self.userList.append(currentUser)
        
    }
    
    fileprivate func editFriendLocaly(_ user: User) {
        var currentUser = self.friendList.filter{ $0.id == user.id }.first!
        currentUser.imageUrl = user.imageUrl
        currentUser.firstName = user.firstName
        currentUser.lastName = user.lastName
        currentUser.phoneNumber = user.phoneNumber
        currentUser.email = user.email
        
        let index = userList.index(of: user)!
        userList[index] = currentUser
        

    }
    
    fileprivate func deleteFriendLocaly(_ user: User) {
        var editedUser = userList.filter{ $0.id == user.id }.first!
        let index = userList.index(of: user)!
        
        editedUser.isFriend = false
        userList[index] = editedUser
        

    }
}

// MARK: - UserInteraction protocol realization
extension DataManager: UserInteraction {

    func add(user: User) {
        addFriendLocaly(user)
        //CoreDataManager.instanse.add(user: user)
        NetworkDataManager.instance.add(user: user)
        
        NotificationCenter.default.post(name: .friendDataUpdated, object: nil)
    }
    
    func edit(user: User) {
        editFriendLocaly(user)
        //CoreDataManager.instanse.edit(user: user)
        NetworkDataManager.instance.edit(user: user)
        
        NotificationCenter.default.post(name: .friendDataUpdated, object: nil)
    }
    
    func delete(user: User) {
        deleteFriendLocaly(user)
        //CoreDataManager.instanse.delete(user: user)
        NetworkDataManager.instance.delete(user: user)
        
    }
}
