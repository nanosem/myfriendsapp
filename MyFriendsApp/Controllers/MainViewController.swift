//
//  MainViewController.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit
import PKHUD

class MainViewController: UITableViewController {

    // MARK: UIView
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.tableView.register(TableViewCell.nibName, forCellReuseIdentifier: TableViewCell.identifier)
        self.tableView.backgroundColor = UIColor(red:0.36, green:0.34, blue:0.37, alpha:1.0)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.friendDataUpdated), name: .friendDataUpdated, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.friendListLoaded), name: .friendListLoaded, object: nil)
        
        fetchUsers()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - TableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataManager.instanse.friendList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        
        let currentFriend = DataManager.instanse.friendList[indexPath.row]

        cell.updateCellData(firstName: currentFriend.firstName, lastName: currentFriend.lastName, imageUrl: currentFriend.imageUrl)
        return cell
    }
    
    // MARK: - TableViewDelegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableViewCell.height
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == .delete) {
            let currentUser = DataManager.instanse.friendList[indexPath.row]
            DataManager.instanse.delete(user: currentUser)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let friends = DataManager.instanse.friendList
        let currentFriend = friends[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        
        let editUserViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditUserViewController") as! EditUserViewController
        editUserViewController.user = currentFriend
        
        self.navigationController?.pushViewController(editUserViewController, animated: true)
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }

    
    // MARK: - Private methods
    
    @IBAction func refreshData(_ sender: Any) { fetchUsers() }
    
    @IBAction func unwindToVC1(segue:UIStoryboardSegue) {}

    
    func animateTable() {
        tableView.reloadData()
        let cells = tableView.visibleCells
        let tableHeight: CGFloat = tableView.bounds.size.height
        
        for currentCell in cells {
            let cell: UITableViewCell = currentCell as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for currentCell in cells {
            let cell: UITableViewCell = currentCell as UITableViewCell
            UIView.animate(withDuration: 1, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveLinear, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: nil)
            
            index += 1
        }
    }
    
    private func fetchUsers() {
        HUD.show(.progress)
        DataManager.instanse.loadFriendList()
    }

    // MARK: - Observed methods 
    func friendDataUpdated() {
        tableView.reloadData()
    }
    
    func friendListLoaded() {
        HUD.hide()
        self.animateTable()
    }
}
