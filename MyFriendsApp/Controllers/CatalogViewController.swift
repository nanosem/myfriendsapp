//
//  CatalogViewController.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import Foundation
import PKHUD

class CatalogViewController: UITableViewController {
    
    private(set) var userList = [User]()
    
    // MARK: - UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(TableViewCell.nibName, forCellReuseIdentifier: TableViewCell.identifier)
        self.tableView.backgroundColor = UIColor(red:0.36, green:0.34, blue:0.37, alpha:1.0)
        
        HUD.show(.progress)
        DataManager.instanse.loadUserList{ [weak self] users in
            self?.userList = users
            HUD.hide()
            self?.animateTable()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.listLoaded), name: .listLoaded, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - UITableViewDataSource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
        
        let user = userList[indexPath.row]
        let firstName = user.firstName
        let lastName = user.lastName
        let imageUrl = user.imageUrl
        
        cell.updateCellData(firstName: firstName, lastName: lastName, imageUrl: imageUrl)
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TableViewCell.height
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let currentUser = userList[indexPath.row]
        DataManager.instanse.add(user: currentUser)
        userList = userList.filter { $0.id != currentUser.id }
        tableView.deleteRows(at: [indexPath], with: .fade)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
    }
    
    // MARK: - Private methods
    private func animateTable() {
        tableView.reloadData()
        let cells = tableView.visibleCells
        let tableHeight: CGFloat = tableView.bounds.size.height
        
        for currentCell in cells {
            let cell: UITableViewCell = currentCell as UITableViewCell
            cell.transform = CGAffineTransform(translationX: 0, y: tableHeight)
        }
        
        var index = 0
        
        for currentCell in cells {
            let cell: UITableViewCell = currentCell as UITableViewCell
            UIView.animate(withDuration: 1, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveLinear, animations: {
                cell.transform = CGAffineTransform(translationX: 0, y: 0)
            }, completion: nil)
            
            index += 1
        }
    }
    
    // MARK: - Observer methods
    func listLoaded() {
        HUD.hide()
        self.animateTable()
    }
}
