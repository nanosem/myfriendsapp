//
//  EditUserViewController.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit
import SwiftValidator
import AlamofireImage
import GoogleMaps
import GoogleMapsCore
import Firebase
import FirebaseStorage
import PKHUD

class EditUserViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - Private fields
    @IBOutlet fileprivate weak var imageBox: UIImageView!
    @IBOutlet fileprivate weak var firstNameTextField: UITextField!
    @IBOutlet fileprivate weak var lastNameTextField: UITextField!
    @IBOutlet fileprivate weak var phoneNumberTextField: UITextField!
    @IBOutlet fileprivate weak var emailTextField: UITextField!
    @IBOutlet fileprivate weak var mainInfoView: UIView!
    @IBOutlet fileprivate weak var mapView: GMSMapView!
    
    fileprivate let validator = Validator()
    
    // MARK: - inits

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.user = User(id: "", imageUrl: URL(string: "T")!, firstName: "", lastName: "", phoneNumber: "", email: "", coordinates: "", isFriend: true)
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.user = User(id: "", imageUrl: URL(string: "T")!, firstName: "", lastName: "", phoneNumber: "", email: "", coordinates: "", isFriend: true)
        super.init(coder: aDecoder)
    }
    
    var user: User
    
    // MARK: - UIView
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red:0.62, green:0.56, blue:0.74, alpha:1.0)
        mainInfoView.backgroundColor = UIColor(red:0.62, green:0.56, blue:0.74, alpha:1.0)
        
        setUpMap()
        
        roundImageBox()
        
        updateViewData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        validator.registerField(self.phoneNumberTextField, rules: [RequiredRule(), PhoneNumberRule(), MinLengthRule(length: 10)])
        validator.registerField(self.emailTextField, rules: [RequiredRule(), EmailRule()])
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        validator.unregisterField(self.phoneNumberTextField)
        validator.unregisterField(self.emailTextField)
    }
    
    // MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag != 3 {
            view.viewWithTag(textField.tag + 1)?.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return true
    }
    
    // MARK: - Private actions
    @IBAction private func hideKeyBoard(_ sender: Any) { view.endEditing(true) }
    
    @IBAction private func done(_ sender: Any) {
        
        validator.validate(self)
        if !validator.errors.isEmpty { /*Warning*/ return }
        
        HUD.show(.progress)

        let imageUrl = NetworkDataManager.instance.uploadImageToStorageForUserWithID(self.user.id, image: self.imageBox.image!)
        
        self.user.imageUrl = imageUrl
        self.user.firstName = (self.firstNameTextField.text)!
        self.user.lastName = (self.lastNameTextField.text)!
        self.user.phoneNumber = (self.phoneNumberTextField.text)!
        self.user.email = (self.emailTextField.text)!
            
        DataManager.instanse.edit(user: self.user)
        
        HUD.hide()
        _ = navigationController?.popViewController(animated: true)

    }
    
    // MARK: - Private functions
    private func updateViewData() {
        imageBox.af_setImage(withURL: user.imageUrl)
        firstNameTextField.text = user.firstName
        lastNameTextField.text = user.lastName
        phoneNumberTextField.text = user.phoneNumber
        emailTextField.text = user.email
    }
    
    private func roundImageBox() {
        imageBox.layer.borderWidth = 0.3
        imageBox.layer.borderColor = UIColor.white.cgColor
        imageBox.layer.cornerRadius = imageBox.frame.width / 2
        imageBox.clipsToBounds = true
    }
    private func setUpMap() {
        mapView.isMyLocationEnabled = true
        mapView.layer.borderColor = UIColor.black.cgColor
        mapView.layer.borderWidth = 3
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(user.location.latitude , user.location.longitude)
        marker.map = mapView
    }
}

// MARK: - UIImagePickerControllerDelegate
extension EditUserViewController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageBox.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction fileprivate func uploadImage(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        let actionSheet = UIAlertController(title: "", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: {[weak self] (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePicker.sourceType = .camera
                self?.present(imagePicker, animated:  true, completion: nil)
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Library", style: .default, handler: {[weak self] (action:UIAlertAction) in
            imagePicker.sourceType = .photoLibrary
            self?.present(imagePicker, animated:  true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }

}

// MARK: - Navigation controller Delegate
extension EditUserViewController: UINavigationControllerDelegate {}

// MARK: - Validation Delegate
extension EditUserViewController: ValidationDelegate {
    func validationSuccessful() {}

    func validationFailed(_ errors:[(Validatable ,ValidationError)]) {
        for (field, error) in errors {
            if let field = field as? UITextField {
                field.layer.borderColor = UIColor.red.cgColor
                field.layer.borderWidth = 1.0
            }
            error.errorLabel?.text = error.errorMessage
            error.errorLabel?.isHidden = false
        }
    }
}
