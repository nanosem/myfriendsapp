//
//  String+Extension.swift
//  MyFriendsApp
//
//  Created by Sem Vastuin on 03.03.17.
//  Copyright © 2017 Sem Vastuin. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
