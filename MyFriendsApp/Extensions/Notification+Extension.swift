//
//  NSNotification+Extension.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let listLoaded = Notification.Name("listLoaded")
    static let friendDataUpdated = Notification.Name("friendDataUpdated")
    static let friendListLoaded = Notification.Name("friendListLoaded")
    static let imageUploaded = Notification.Name("imageUploaded")
}
