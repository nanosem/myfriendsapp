//
//  TableViewCell.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    // MARK: -
    static let identifier = "\(TableViewCell.self)"
    static let nibName = UINib(nibName: "\(TableViewCell.self)", bundle: nil)
    static let height: CGFloat = 75

    // MARK: - Private fields
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var imageBox: UIImageView!

    // MARK: - UITableViewCell
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.updateCellConfig()
    }
    
    func updateCellData(firstName: String, lastName: String, imageUrl: URL) {
        self.imageBox.af_setImage(withURL: imageUrl)
        self.nameLabel.text = "\(firstName) \(lastName)"
    }
    
    // MARK: - Cell configuration
    private func updateCellConfig() {
        nameLabel.textColor = UIColor.white
        nameLabel.font = UIFont(name: nameLabel.font.fontName, size: 20)
        
        imageBox.layer.borderWidth = 0.3
        imageBox.layer.borderColor = UIColor.white.cgColor
        
        imageBox.layer.cornerRadius = imageBox.frame.width / 2
        imageBox.clipsToBounds = true
    }
}
