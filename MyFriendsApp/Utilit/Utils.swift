//
//  Utils.swift
//  MyFriends
//
//  Created by a on 01.03.17.
//  Copyright © 2017 nanosem. All rights reserved.
//

import Foundation
import GoogleMaps
import GoogleMapsCore

struct Utils {
    
    static var randomLocation: CLLocationCoordinate2D {
        let latitude = Double(arc4random_uniform((500)-250))
        let loungitude = Double(arc4random_uniform((500)-250))
        
        return CLLocationCoordinate2DMake(latitude, loungitude)
    }
    
    // MARK: - Generate random string
    static var randomString: String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< 20 {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        return randomString
    }
}
