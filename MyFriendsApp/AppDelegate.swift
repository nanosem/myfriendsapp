//
//  AppDelegate.swift
//  MyFriendsApp
//
//  Created by Sem Vastuin on 02.03.17.
//  Copyright © 2017 Sem Vastuin. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps
import GoogleMapsCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // FireBase
        FIRApp.configure()
        
        // GoogleMaps
        GMSServices.provideAPIKey("AIzaSyCtRLgik7Q6pO15gnXCH3LxG-SH0JS-SY8")
        
        // UI Configuration
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor(netHex: 0xffffff)
        navigationBarAppearace.barTintColor = UIColor(red: 95, green: 26, blue: 112)
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    
    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {
        CoreDataManager.instanse.saveContext()
    }
}
